FROM openjdk:8-jre-alpine

RUN mkdir /code
COPY build/libs /code

ENTRYPOINT [ "sh", "-c", "java -jar -Dnetworkaddress.cache.ttl=600 -Dnetworkaddress.cache.negative.ttl=5 /code/trii-challenge.jar" ]