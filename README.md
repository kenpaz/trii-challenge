# trii-challenge

#Stack Tech
* Kotlin
* Java 8
* Gradle
* Spring Boot 2.2.2.RELEASE
* Feign

# Requirements
 * Java 8+ (for jar execution)
 * Docker

__Building jar__

mac os/linux `./gradlew build`

windows `gradlew build`

__Building Docker image__

1. Required execute jar package previously

`docker build -t <name> .`

# Usage
__As jar__

`java -jar trii-chalenge.jar`

__As Docker image__

`docker run -p 8080:8080 <name>`