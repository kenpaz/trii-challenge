package com.trii.challange

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@EnableFeignClients
@ComponentScan("com.trii.challange")
class Application

fun main(args: Array<String>) {
	SpringApplication.run(Application::class.java, *args)
}
