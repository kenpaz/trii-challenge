package com.trii.challange.client

import com.trii.challange.configuration.FeignConfiguration
import com.trii.challange.constant.Constant
import com.trii.challange.environment.ChallengeEnvironment
import com.trii.challange.response.StockResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(name = "FinancialClient", configuration = [FeignConfiguration::class], url = "https://financialmodelingprep.com/api/v3")
interface FinancialClient {

    @GetMapping("/quote-short/{${Constant.PathParam.STOCK_NAME}}")
    fun getStockPrice(@PathVariable(Constant.PathParam.STOCK_NAME) stockName: String,
                      @RequestParam("apikey") apiKey: String = ChallengeEnvironment.financialApiKey): List<StockResponse>
}