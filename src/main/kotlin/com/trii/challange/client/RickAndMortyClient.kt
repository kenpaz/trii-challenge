package com.trii.challange.client

import com.trii.challange.configuration.FeignConfiguration
import com.trii.challange.constant.Constant
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import javax.websocket.server.PathParam

@FeignClient(name = "RickAndMortyClient", configuration = [FeignConfiguration::class], url = "https://rickandmortyapi.com/api")
interface RickAndMortyClient {

    @GetMapping("/character")
    fun getCharacter(@RequestParam(Constant.QueryParam.PAGE) page: String,
                     @RequestParam(Constant.QueryParam.NAME) name: String,
                     @RequestParam(Constant.QueryParam.STATUS) status: String): String

    @GetMapping("/character")
    fun getCharacter(@PathParam(Constant.PathParam.ID) id: Int): String
}