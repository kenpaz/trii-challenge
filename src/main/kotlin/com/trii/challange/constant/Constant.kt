package com.trii.challange.constant

object Constant {

    val STOCK_LIST = listOf("AAPL", "GOOGL", "AMZN", "TSLA", "FB", "TWTR", "UBER", "LYFT", "SNAP", "SHOP")

    object QueryParam {
        const val PAGE = "page"
        const val NAME = "name"
        const val STATUS = "status"
    }

    object PathParam {
        const val ID = "id"
        const val STOCK_NAME = "stock_name"
    }
}