package com.trii.challange.constant

object Route {
    const val V1 = "/api/v1"
    const val CHARACTER = "/character"
    const val DOWNLOAD_ZIP = "/download-zip"
}