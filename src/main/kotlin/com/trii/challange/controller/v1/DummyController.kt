package com.trii.challange.controller.v1

import com.trii.challange.constant.Constant
import com.trii.challange.constant.Route
import com.trii.challange.service.DummyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.util.FileCopyUtils
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.nio.charset.StandardCharsets
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping(value = [(Route.V1)], produces = [MediaType.APPLICATION_JSON_VALUE])
class DummyController {

    @Autowired
    private lateinit var dummyService: DummyService

    @GetMapping(Route.CHARACTER)
    fun getCharacter(
        @RequestParam(Constant.QueryParam.PAGE, required = true) page: String,
        @RequestParam(Constant.QueryParam.NAME, required = true) name: String,
        @RequestParam(Constant.QueryParam.STATUS, required = true) status: String
    ): ResponseEntity<String> = ResponseEntity.ok(dummyService.getCharacter(page, name, status))

    @GetMapping(Route.DOWNLOAD_ZIP)
    fun download(response: HttpServletResponse) {
        val responseBytes = dummyService.createZip()

        response.contentType = "application/zip"
        response.setContentLength(responseBytes.size)
        response.addHeader("Content-Disposition", "attachment; ${String.format("filename*=${StandardCharsets.UTF_8.name()}''%s", "response.zip")}")

        val outputStream = response.outputStream
        FileCopyUtils.copy(responseBytes, outputStream)
        outputStream.close()
    }
}