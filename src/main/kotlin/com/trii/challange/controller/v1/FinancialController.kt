package com.trii.challange.controller.v1

import com.trii.challange.constant.Route
import com.trii.challange.service.FinancialService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@RestController
@RequestMapping(value = [(Route.V1)], produces = [MediaType.APPLICATION_JSON_VALUE])
class FinancialController {

    @Autowired
    private lateinit var financialService: FinancialService

    @GetMapping("/investment")
    fun calculateInvestment(): ResponseEntity<BigDecimal> {
        return ResponseEntity.ok(financialService.calculateInvestment())
    }
}