package com.trii.challange.environment

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration("ChallengeEnvironment")
class ChallengeEnvironment {

    companion object {
        var financialApiKey: String = ""
        var appName: String = ""
    }

    @Value("\${financial.api.key}")
    fun setFinancialApiKey(environmentKey: String) {
        financialApiKey = environmentKey
    }

    @Value("\${spring.application.name}")
    fun setAppName(environmentKey: String) {
        appName = environmentKey
    }
}