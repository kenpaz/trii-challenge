package com.trii.challange.service

import com.trii.challange.client.RickAndMortyClient
import com.trii.challange.environment.ChallengeEnvironment
import com.trii.challange.util.ZipUtil
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DummyService {

    @Autowired
    private lateinit var rickAndMortyClient: RickAndMortyClient

    companion object {
        private val CLASS = DummyService::class.java.simpleName
    }
    private val logger = LoggerFactory.getLogger(DummyService::class.java)

    fun getCharacter(page: String, name: String, status: String): String {
        logger.info("--{} --$CLASS:getCharacter", ChallengeEnvironment.appName)
        return rickAndMortyClient.getCharacter(page, name, status)
    }

    fun createZip(): ByteArray {
        logger.info("--{} --$CLASS:createZip", ChallengeEnvironment.appName)
        val json = rickAndMortyClient.getCharacter(1)
        return ZipUtil.create(json.toByteArray())
    }
}