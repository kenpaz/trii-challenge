package com.trii.challange.service

import com.trii.challange.client.FinancialClient
import com.trii.challange.constant.Constant
import com.trii.challange.environment.ChallengeEnvironment
import com.trii.challange.response.StockResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.concurrent.CompletableFuture
import java.util.stream.Collectors

@Service
class FinancialService {

    @Autowired
    private lateinit var financialClient: FinancialClient

    companion object {
        private val CLASS = FinancialService::class.java.simpleName
    }

    private val logger = LoggerFactory.getLogger(FinancialService::class.java)

    //TODO: the financial services throws 429 status
    fun calculateInvestment(): BigDecimal {
        logger.info("--{} --$CLASS:calculateInvestment", ChallengeEnvironment.appName)
        val sum = BigDecimal.ZERO

        Constant.STOCK_LIST.forEach {
            sum.add(financialClient.getStockPrice(it)[0].price)
        }

        return sum
    }

    //TODO: the financial services throws 429 status
    fun calculateInvestmentConcurrent(): BigDecimal {
        logger.info("--{} --$CLASS:calculateInvestment", ChallengeEnvironment.appName)
        val futures: List<CompletableFuture<List<StockResponse>>> = Constant.STOCK_LIST.stream()
            .map{ name -> getStockPrice(name) }
            .collect(Collectors.toList())

        val allFutures = CompletableFuture.allOf(*futures.toTypedArray())

        val allContentFutures = allFutures.thenApply {
            return@thenApply futures.stream()
                .map {  content -> content.join() }
                .collect(Collectors.toList())
        }

        val sumFuture = allContentFutures.thenApply { contents ->
            return@thenApply contents.stream()
                .map { list -> list[0].price }
                .reduce(BigDecimal::add).get()
        }

        return sumFuture.get()
    }

    @Async
    fun getStockPrice(stock: String): CompletableFuture<List<StockResponse>> {
        return CompletableFuture.completedFuture(financialClient.getStockPrice(stock))
    }
}