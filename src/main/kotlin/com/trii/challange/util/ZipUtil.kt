package com.trii.challange.util

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

object ZipUtil {
    
    fun create(json: ByteArray): ByteArray {
        return ByteOutputStream().use { fout ->
            ZipOutputStream(fout).use { zout ->
                zout.setLevel(1)
                val ze = ZipEntry("response.json")
                ze.size = json.size.toLong()
                zout.putNextEntry(ze)
                zout.write(json)
                zout.closeEntry()
                fout.bytes
            }
        }
    }
}